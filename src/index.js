import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
// import GMap from './GMap'
// import MapWithADirectionsRenderer from './GMap2'
import MapComponent from './GMap3'
ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    {/* <GMap /> */}

  {/* <MapWithAMarker
    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNpl-g_PXWP5mW2i44ySwpNuygZryO8zY&v=3.exp&libraries=geometry,drawing,places"
    loadingElement={<div style={{ height: `100%` }} className='loadingElement'/>}
    containerElement={<div style={{ height: `100vh` }} className='containerElement'/>}
    mapElement={<div style={{ height: `100%` }} className='mapElement'/>}
  /> */}
  {/* <MapWithADirectionsRenderer /> */}
  <MapComponent />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
