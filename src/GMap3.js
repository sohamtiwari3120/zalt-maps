/*global google*/
import React from 'react';
import routes from './routes.json'
import {withScriptjs, withGoogleMap, GoogleMap, Marker, Polyline, DirectionsRenderer } from "react-google-maps";
const { compose, withProps, lifecycle } = require("recompose");

class Map extends React.Component {
    constructor(props)
    {
        super(props);
        this.state={
            flag:false,
            directionService:[]
        }
    }
    path = [
        { lat: 18.558908, lng: -68.389916 },
        { lat: 18.558853, lng: -68.389922 },
        { lat: 18.558375, lng: -68.389729 },
        { lat: 18.558032, lng: -68.389182 },
        { lat: 18.55805, lng: -68.388613 },
        { lat: 18.558256, lng: -68.388213 },
        { lat: 18.558744, lng: -68.387929 }
      ];
    innerRoutes=routes['routes']
    componentDidMount = ()=>
    {
        const DirectionsService = new google.maps.DirectionsService();
        // var driver = "Moti Karpe";
        for(var driver in this.innerRoutes)
        {
            var waypts=[]
            for(var i = 1; i<this.innerRoutes[driver].length-1;i++)
            {
                waypts.push(
                    {
                        location: {lat: this.innerRoutes[driver][i][0], lng: this.innerRoutes[driver][i][1]},
                        stopover:true
                    }
                )
            }
            DirectionsService.route({
                origin: new google.maps.LatLng(this.innerRoutes[driver][0][0], this.innerRoutes[driver][0][1]),
                destination: new google.maps.LatLng(this.innerRoutes[driver][this.innerRoutes[driver].length-1][0], this.innerRoutes[driver][this.innerRoutes[driver].length-1][1]),
                waypoints: waypts,
                optimizeWaypoints: true,
                travelMode: google.maps.TravelMode.DRIVING,
              }, (result, status) =>
                {
                    if (status === google.maps.DirectionsStatus.OK)
                    {
                        this.setState({directionService:this.state.directionService.concat(result), flag:true})
                    }
                    else
                    {
                        console.error(`error fetching directions ${result}`);
                    }
                }
            );
        }

    }
    render = () =>
    {
        var color='#ff0000'
        var pathsRendered=null;
        if(this.state.flag===true)
        {
            pathsRendered=this.state.directionService.map((direction, i)=>{
                color='#'+Math.floor(Math.random()*16777215).toString(16);
                return <DirectionsRenderer directions={direction} options={{polylineOptions:{strokeColor: color},suppressMarkers:false, markerOptions:{
                    icon:{
                        path:google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                        strokeColor:color,
                        scale:3
                    }
                }}}/>});
            // pathsRendered=<DirectionsRenderer directions={this.state.directionService
        }
        return (
          <GoogleMap
            defaultZoom={12}
            defaultCenter={{ lat: 13.0827, lng: 80.2707 }}
          >
            {/* <Polyline path={this.path} options={{ strokeColor: "#00f2ff ", strokeWeight:10 }} /> */}
            {pathsRendered}
          </GoogleMap>
        );
    };
}

const MapComponent = withScriptjs(withGoogleMap(Map));

export default () => (
  <MapComponent
    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNpl-g_PXWP5mW2i44ySwpNuygZryO8zY&v=3.exp&libraries=geometry,drawing,places"
    loadingElement={<div style={{ height: `100%` }} />}
    containerElement={<div style={{ height:'100vh' }} />}
    mapElement={<div style={{ height: `100%` }} />}
  />
);
